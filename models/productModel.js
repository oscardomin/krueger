// app/models/productModel.js

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var ProductSchema   = new Schema({
    _id : { type: String, unique: true },
    name : String,
    description : String,
    imageURL: String,
    category: String,
    subcategory: String,
    prices: {
        alcampo : Number,
        caprabo : Number,
        carrefour : Number,
        condis : Number,
        eroski : Number,
        elcorteingles : Number,
        hipercor : Number,
        mercadona : Number
    }
});

module.exports = mongoose.model('Product', ProductSchema);