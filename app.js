var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var mainRouttes = require('./routes/index');

var app = express();

app.use(express.static(path.join(__dirname, 'public')));


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// connect to our database
var mongoose = require('mongoose');
mongoose.connect(process.env.MONGOLAB_URI, function(err) {
    if(err) {
        console.log('Failed to connect to MongoDB. (Maybe \'mongod\' command is not running?)');
        throw err;
    }
    console.log('Connection to MongoDB established.');
});

// uncomment after placing your favicon in /public

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('node-compass')({mode: 'expanded'}));
process.env.PWD = process.cwd();

app.use(favicon(process.env.PWD+'/public/images/favicon.ico'));

app.use('/', mainRouttes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {
            status: err.status,
            stack: err.stack
        }
    });
});

module.exports = app;
