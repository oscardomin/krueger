var express = require('express');
var router = express.Router();
var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');
var Product = require('../models/productModel');

//GET home page.
router.get('/', function(req, res) {
    res.render('index');
});

//To extract the price of a concrete supermarket of a given product
function getPrice(html, supermarket) {
    var $ = cheerio.load(html);
    var price;
    switch (supermarket) {
        case "alcampo":
            price = $(".table-cuanto-vale .super-alcampo + .price > p > strong");
            break;
        case "caprabo":
            price = $(".table-cuanto-vale .super-caprabo + .price > p > strong");
            break;
        case "carrefour":
            price = $(".table-cuanto-vale .super-carrefour + .price > p > strong");
            break;
        case "condis":
            price = $(".table-cuanto-vale .super-condis + .price > p > strong");
            break;
        case "eroski":
            price = $(".table-cuanto-vale .super-eroski + .price > p > strong");
            break;
        case "elcorteingles":
            price = $(".table-cuanto-vale .super-elcorteingles + .price > p > strong");
            break;
        case "hipercor":
            price = $(".table-cuanto-vale .super-hipercor + .price > p > strong");
            break;
        case "mercadona":
            price = $(".table-cuanto-vale .super-mercadona + .price > p > strong");
            break;
        default:
            break;
    }
    return (Number(price.text().replace(',','.').split("€")[0]));
}

//Array with all the URLs of the Products.
productURLs = [];

router.post('/scrape', function(req, res) {

    if(productURLs.length <= 0) {
        console.error('ERROR: There are any URLs of products to extract data.');
        res.json({
            message: 'There are any URLs of products to extract data. :\'('
        });
    } else {
        for (var i = 0; i < productURLs.length; i++) {
            (function(url) {
                request(url, function (error, response, html) {
                    if (!error) {
                        var $ = cheerio.load(html);
                        // We'll use the unique header class as a starting point.

                        var product = new Product();
                        var id = url.split("/producto/")[1];

                        product.name = $('.marca').text();
                        product.description = $('.formato').text();
                        product.imageURL = "www.carritus.com" + $('.image > img').attr('src');
                        product.prices.alcampo = getPrice(html, "alcampo");
                        product.prices.caprabo = getPrice(html, "caprabo");
                        product.prices.carrefour = getPrice(html, "carrefour");
                        product.prices.condis = getPrice(html, "condis");
                        product.prices.eroski = getPrice(html, "eroski");
                        product.prices.elcorteingles = getPrice(html, "elcorteingles");
                        product.prices.hipercor = getPrice(html, "hipercor");
                        product.prices.mercadona = getPrice(html, "mercadona");
                        product.category = $(".breadcrumb .btn-toolbar > .btn-group:nth-child(3) > a").text();
                        product.subcategory = $(".breadcrumb .btn-toolbar > .btn-group:nth-child(4) > a").text();

                        Product.findByIdAndUpdate(id, {$set: product}, {upsert: true}, function (err) {
                            if (!err) {
                                console.log('Product from ' + url + ' created/updated!');
                            }
                            else {
                                console.error('ERROR:' + err);
                            }
                        });
                    } else {
                        console.error('ERROR:' + error);
                    }
                });
            })(productURLs[i]);

        }
        res.json({
            message: 'Petition to extract products sent!'
        });
    }
});

//Given an URL of a subcategory last-level, krueger extracts the URLs of all the products
function getURLproducts (url) {
    (function(url) {
        request(url, function(error, response, html) {
            if(!error) {
                var $ = cheerio.load(html);
                // We'll use the unique header class as a starting point.

                var subcats = $(".products-list > .item > .image > a");
                subcats.each(function() {
                    if(productURLs.indexOf('http://www.carritus.com' + $(this).attr('href')) < 0) {
                        productURLs.push('http://www.carritus.com' + $(this).attr('href'));
                    }
                });
                console.log(productURLs.length);
            }
        });
    })(url);
}

function getLastCategoryURLs (url) {
    (function(url) {
        request(url, function(error, response, html) {
            if(!error) {
                var $ = cheerio.load(html);
                // We'll use the unique header class as a starting point.
                var cats = $(".acordeon .item-product a");
                var currentURL = 'http://www.carritus.com' + cats.first().attr('href');
                var isLastCategory = (currentURL == url);
                if(isLastCategory) {
                    cats.each(function() {
                        getURLproducts('http://www.carritus.com' + $(this).attr('href'));
                    })
                } else {
                    cats.each(function() {
                        getLastCategoryURLs('http://www.carritus.com' + $(this).attr('href'));
                    })
                }
            }
        });
    })(url)
}

router.get('/products', function(req, res) {
    var startURL = 'http://www.carritus.com/tienda/super/mercadona/cp/08016/cm/';
    getLastCategoryURLs(startURL + '1985');
    res.json({ message: 'Products\'  URLs extracted!' });
});



module.exports = router;
